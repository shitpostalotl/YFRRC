# Your favorite robot Reddit channel

An automated Reddit reading video generator, because origionality is dead and we have sold it's grave. Run the script without any arguments (or look at line 5) for a description of proper usage. If you do not have Nix/NixOS installed, you will have to edit the shabangs and install dependancies manually.

If you want to add music to the video, put it in the root directory of the repo as `music.mp3`. A known good music track on YouTube is `cscuCIzItZQ`.

## Usage
usage: curl [url] | $0 [count] [music] [volume-tts] [volume-music] [-p]

The main input is stdin (the thing you pass with a pipe), which is a post on a teddit instance such as https://teddit.bus-hit.me/r/askreddit/comments/121joyd/?api (don't just put the url through stdin). The video and thumbnail are at assets/out.mp4 and assets/0.png. The program may fail randomly.

[count] is the amount of comments you want to scrape

[music] is the path to the music file you want to play in the background.

[volume-tts] and [volume-music] are optional arguments that determine the volume multiplier for their respective audio tracks

if -p is present as the final argument, it will show progress messages.

example: curl -s https://teddit.bus-hit.me/r/askreddit/comments/121joyd/?api | ./makeVideo.sh 3 music.mp3 10 0.25 -p
