#! /usr/bin/env nix-shell
#! nix-shell -i bash -p jq imagemagick mimic ffmpeg

# argument and pipe processing
[[ $# -eq 0 ]] && { echo -e "usage: curl [url] | $0 [count] [music] [volume-tts] [volume-music] [-p]\nThe main input is stdin (the thing you pass with a pipe), which is a post on a teddit instance such as https://teddit.bus-hit.me/r/askreddit/comments/121joyd/?api (don't just put the url through stdin). The video and thumbnail are at assets/out.mp4 and assets/0.png. The program may fail randomly.\n[count] is the amount of comments you want to scrape\n[music] is the path to the music file you want to play in the background.\n[volume-tts] and [volume-music] are optional arguments that determine the volume multiplier for their respective audio tracks\nif -p is present as the final argument, it will show progress messages.\nexample: curl -s https://teddit.bus-hit.me/r/askreddit/comments/121joyd/?api | ./makeVideo.sh 3 music.mp3 10 0.25 -p"; exit; }
postData=$(cat -)
echo $postData | jq . &> /dev/null || { echo "please pass valid json data"; exit; }
mkdir assets 2> /dev/null || { echo "assets folder still present! move or delete it"; exit; }
music=$(pwd)/$2

# genorate assets
cd assets
for i in $(seq 0 $1); do
	[[ "${@: -1}" = "-p" ]] && echo "[$i/$1]"
	body=$([[ $i -eq 0 ]] && echo $postData | jq -r .title || echo $postData | jq -r .comments[$i].body_html | sed "s/\&nbsp\;/ /g;s/\&lt\;/\</g;s/\&gt\;/\>/g;s/\&amp\;/\&/g;s/\&quot\;/i\\\"/g;s/&apos;/'/g;s/\&\#39\;/\'/g;s/\&cent\;/¢/g;s/\&pound\;/£/g;s/\&yen\;/¥/g;s/\&euro\;/€/g;s/\&copy\;/©/g;s/\&reg\;/®/g;s/<[^>]*>//g")
	author=$(echo $postData | jq -r $([[ $i -eq 0 ]] || echo ".comments[$i]").author)
	magick -background black -fill white -font Comfortaa-Regular -size 1740x904 caption:"$body" $i"body.png"
	magick -background black -fill grey -font Comfortaa-Regular -size 1740x176 caption:"u/$author" $i"author.png"
	convert $i"author.png" $i"body.png" -append $i"text.png"
	[[ $i -eq 0 ]] && convert ../banner.png $i"text.png" +append $i.png || mv $i"text.png" $i.png
	mimic "$body" $i.wav
	ffmpeg -loop 1 -i $i.png -i $i.wav -ss 00:00:00 -t $(ffmpeg -v quiet -stats -i $i.wav -f null - 2> /dev/stdout | tr ' ' '\n' | sed -n 9p | sed 's/^.\{5\}//') $i.mp4 -hide_banner -loglevel error
	echo "file $i.mp4" >> fl
done

# compile video
[[ "${@: -1}" = "-p" ]] && echo "concat phase..."
ffmpeg -f concat -i fl -stream_loop -1 -i $music -max_interleave_delta 50000 -c:v copy -filter_complex "[0:a] volume=${3:-10} [tts]; [1:a] volume=${4:-0.25} [music]; [music][tts] amix=inputs=2:duration=shortest [audio_out]" -map 0:v -map "[audio_out]" out.mp4 -hide_banner -loglevel error
